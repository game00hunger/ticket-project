package com.example.ticketstest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
public class TicketsTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(TicketsTestApplication.class, args);
	}

}
