package com.example.ticketstest.sources;

public class TicketResponce {

    private String id;
    private String eventName;
    private Integer type;
    private Float price;
    private Float priceWithNds;

    public TicketResponce(String id, String eventName, Integer type, Float price){
        this.id = id;
        this.eventName = eventName;
        this.type = type;
        this.price = price;
        this.priceWithNds = price*1.2f;
    }

    public TicketResponce(){

    }

    public String getId() {
        return id;
    }

    public String getEventName() {
        return eventName;
    }

    public Integer getType() {
        return type;
    }

    public Float getPrice() {
        return price;
    }

    public Float getPriceWithNds() {
        return priceWithNds;
    }
}
