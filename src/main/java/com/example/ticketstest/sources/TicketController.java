package com.example.ticketstest.sources;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;


@RestController
@RequestMapping("/tickets")
public class TicketController  {

     @Autowired
     private TickRepo tickRepo;

     @Autowired
     private Converter converter;


    @PostMapping
    public TicketResponce Add(@RequestBody TicketRequest req) throws SQLException{
        TicketEntity entity = converter.fromRequestToEntity(req);
        tickRepo.save(entity);
        return converter.fromEntityToResponce(java.util.Optional.ofNullable(entity));
    }

    @GetMapping
    public TicketResponce GET(String id) throws SQLException {
            return converter.fromEntityToResponce(tickRepo.findById(id));
    }

}
