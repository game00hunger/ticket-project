package com.example.ticketstest.sources;


import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;


public interface TickRepo extends CrudRepository<TicketEntity, String> {

}
