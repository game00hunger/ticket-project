package com.example.ticketstest.sources;


import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class Converter {

    public TicketResponce fromEntityToResponce(Optional<TicketEntity> entity){
        return new TicketResponce(entity.get().getId(), entity.get().getEventName(), entity.get().getType(), entity.get().getPrice());
    }

    public TicketEntity fromRequestToEntity(TicketRequest request){
        return new TicketEntity(request.getEventName(), request.getType(), request.getPrice());
    }

}
