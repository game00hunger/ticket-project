package com.example.ticketstest.sources;

import org.springframework.context.event.EventListener;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import java.rmi.server.UID;
import java.util.Random;
import java.util.UUID;

@Entity
@Table(name = "entities")
public class TicketEntity {

    @Id
    @Column(name = "event_id")
    private String id;

    @Column(name = "event_name")
    private String eventName;
    @Column(name = "event_type")
    private Integer type;
    @Column(name = "price")
    private Float price;

    public TicketEntity(String eventName, Integer type, Float price){
        this.id = UUID.randomUUID().toString();
        this.eventName = eventName;
        this.type = type;
        this.price = price;
    }

    public TicketEntity(){

    }

    public String getEventName() {
        return eventName;
    }

    public Integer getType() {
        return type;
    }

    public Float getPrice() {
        return price;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public String getId() {
        return id;
    }

}
